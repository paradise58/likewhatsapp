package com.paradise.likewhatsapp;

public class UserObject {
    String name,
            phone;


    public UserObject(String name, String phone) {

        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }
}
